import metaData from './static/data/metaData';
import photoStreamData from './static/data/photoStreamData.json';
let dynamicRoutes = () => {
	return new Promise(resolve => {
		resolve(photoStreamData.map(el =>`/${el.photoStreamType}/${el.photoStreamView}`));
	});
};

export default {
	mode: 'universal',

	/**
	 ** Headers of the page
	 */
	head: {
		title: metaData.default.title,
		titleTemplate: null,
		meta: [
			{ charset: 'utf-8' },
			{ name: 'viewport', content: 'width=device-width, initial-scale=1' },
			{ hid: 'description', name: 'description', content: metaData.default.description },
			{ hid: 'keywords', name: 'keywords', content: metaData.default.keywords },
		],
		link: [
			{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
		]
	},

	/**
	 ** Customize the progress-bar color
	 */
	loading: { color: '#fff' },

	/**
	 ** Global CSS
	 */
	css: [
		'@/assets/theme/sass/tlg-theme.scss'
	],

	router: {
		linkActiveClass: 'tlg-active-link',
		linkExactActiveClass: 'tlg-exact-active-link'
	},

	/**
	 ** Plugins to load before mounting the App
	 */
	plugins: [
	],

	/**
	 ** Nuxt.js modules
	 */
	modules: [
		'@nuxtjs/pwa',
		['vue-scrollto/nuxt', { duration: 500 }],
	],

	/**
	 ** Build configuration
	 */
	build: {
		/**
		 ** You can extend webpack config here
		 */
		assetsPublicPath: '/',
		assetsSubDirectory: 'static',
		extend(config, ctx) {
		}
	},
	generate: {
		routes: dynamicRoutes

		// routes: [
		// 	'/gallery/autumn',
		// 	'/gallery/landscape',
		// 	'/gallery/studio',
		// 	'/gallery/people',
		// 	'/gallery/things',
		// 	'/gallery/urban',
		// 	'/albums/iceland',
		// 	'/albums/capeverde',
		// 	'/albums/morocco',
		// 	'/albums/scotland',
		// 	'/albums/budapest',
		// 	'/albums/krivan'
		// ]
	},
};
