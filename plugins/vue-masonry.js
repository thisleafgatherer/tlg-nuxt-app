// ==================================================================
// ==================== VUE MASONRY PLUGIN ==========================
// ==================================================================

/*
* @DOCS: https://www.npmjs.com/package/vue-masonry
*/

// ===== IMPORT =================================
import Vue from 'vue';
import {VueMasonryPlugin} from 'vue-masonry';

// ===== USE ====================================
Vue.use(VueMasonryPlugin);