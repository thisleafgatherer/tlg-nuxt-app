# tlg-nuxt-app

> This Leaf Gatherer re-built with Nuxt.js

## Build Setup

``` bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:3000
$ yarn run dev

# build for production and launch server
$ yarn run build
$ yarn start

# generate static project
$ yarn run generate
```

## Production URL

[thisleafgatherer.com](http://www.thisleafgatherer.com)

For detailed explanation on how things work, checkout [Nuxt.js docs](https://nuxtjs.org).
