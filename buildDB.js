const fs = require('fs')
const photoStreamData = require('./static/data/photoStreamData.json')
const photoStreamDataArr = []


const enforceWaiting = () => {
	return new Promise(resolve => setTimeout(resolve, 0))
};

const readDirectory = async (item) => {
	const dir = `./assets/img/${item.photoStreamType}/${item.photoStreamView}/full/large`
	await fs.readdir(dir, (err, images) => {
		const updatedItem = Object.assign(item, {"images": images})
		photoStreamDataArr.push(updatedItem)
	});
	await enforceWaiting()
};

const iterateJson = async (json) => {
	for (const item of json) {
		await readDirectory(item)
	}
};

const createJsonFile = () => {
	const json = JSON.stringify(photoStreamDataArr);
	fs.writeFile('./static/data/photoStreamAdvancedData.json', json, 'utf8', (err) => console.log(err))
};

iterateJson(photoStreamData).then(() => createJsonFile())
